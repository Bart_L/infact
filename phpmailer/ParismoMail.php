<?php

/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 2016-02-13
 * Time: 11:22
 */
class ParismoMail
{
    /**
     * PHPMailer object.
     *
     * @var PHPMailer
     */
    protected $mail;

    public function __construct()
    {
        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        $this->mail->Host = 'sevencomp.nazwa.pl';
        $this->mail->SMTPAuth = true;
        $this->mail->Username = 'testy@sevencomp.nazwa.pl';
        $this->mail->Password = 'Qwerty12';
        $this->mail->SMTPSecure = 'tls';
        $this->mail->Port = 587;
        $this->mail->CharSet = 'UTF-8';
        $this->mail->isHTML(true);
    }

    /**
     * Sets from mail parameter.
     *
     * @param $email
     * @param string $name
     * @throws phpmailerException
     */
    public function setFrom($email = "zorza@spolem.org.pl", $name = "Hotel ZORZA")
    {
        $this->mail->setFrom($email, $name);
    }

    /**
     * Sets email recipients list.
     *
     * @param $email
     * @param string $name
     */
    public function addRecipient($email, $name = "")
    {
        $this->mail->addAddress($email, $name);
    }

    /**
     * Sets reply to email.
     *
     * @param $email
     * @param string $name
     */
    public function setReplyTo($email = "zorza@spolem.org.pl ", $name = "Hotel ZORZA")
    {
        $this->mail->addReplyTo($email, $name);
    }

    /**
     * Sets email subject.
     *
     * @param string $subject
     */
    public function setSubject($subject = "")
    {
        $this->mail->Subject = $subject;
    }

    /**
     * Sets message template and optional attach parameters.
     *
     * @param $template
     * @param array $search
     * @param array $replace
     */
    public function setMessage($template, $search = array(), $replace = array())
    {
        $message = file_get_contents($template);

        if (sizeof($search) > 0 && sizeof($replace) > 0) {
            $message = str_replace($search, $replace, $message);
        }

        $this->mail->msgHTML($message);
    }

    /**
     * Send email message.
     *
     * @return bool
     * @throws Exception
     * @throws phpmailerException
     */
    public function sendEmail()
    {
        if (!$this->mail->send()) {
            throw new Exception($this->mail->ErrorInfo);
        } else {
            return true;
        }
    }

}

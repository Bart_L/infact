<?php
require_once('vendor/autoload.php');
require_once('phpmailer/ParismoMail.php');

$name = $_POST['name'];
$email = $_POST['email'];
$title = $_POST['surname'];
$message = $_POST['message'];

$replace = array("{{osoba_kontaktowa}}", "{{email}}", "{{message}}", "{{temat}}");
$search = array($name, $email, $message, $title);

$ma = new ParismoMail();
$ma->setSubject("Infact kontakt");
$ma->setFrom("testy@sevencomp.pl", "Sevencomp.pl");
$ma->setMessage('phpmailer/formularz.html', $replace, $search);
$ma->addRecipient("biuro@infact.com.pl", "Biuro Infact");
$ma->sendEmail();

$cookie_name = "form";
$cookie_value = "true";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

header ("Location: index.html");
